 
<h1 align="center" style="margin: 10px 10px; font-weight: bold;margin-top: 0 !important">FlowDesigner</h1> 
<h4 align="center">FlowDesigner是基于Vue3 + element-plus 的前端工作流设计新模式 </h4>
 
 
-  **FlowDesigner流程设计器** 【在线预览】<a href="http://117.72.70.166/ant-flow/dist/" target="_blank">http://117.72.70.166/ant-flow/dist/</a>

-  **FlowDesigner成品案例** 【在线预览】<a href="http://117.72.70.166/admin/" target="_blank">http://117.72.70.166/admin/</a>
- 开源前端地址 
<a href="https://gitcode.com/ldhnet/FlowDesigner/" target="_blank">GitCode仓库</a>
- QQ技术交流群（972107977） 期待您的加入
- 有疑问可以Issues留言，我们会认真对待 

<table> 
  <thead>
    <tr>
      <th scope="col">源码地址</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>
        <a href="https://gitcode.com/ldhnet/FlowDesigner" target="_blank">
           https://gitcode.com/ldhnet/FlowDesigner
        </a>
      </th>
    </tr> 
    <tr> 
      <th>
        <a href="https://gitee.com/ldhnet/AntFlow-Vue3" target="_blank">
            https://gitee.com/ldhnet/AntFlow-Vue3
        </a>
      </th> 
    </tr>  
        <tr> 
      <th>
        <a href="https://gitee.com/ldhnet/AntFlow-Vue3" target="_blank">
            https://github.com/ldhnet/AntFlow-Vue3
        </a>
      </th> 
    </tr>  
  </tbody> 
</table>
 
## 项目介绍

- 核心技术栈<br />
 Vue3、vue-router、 Pinia、
 element-plus、 vite、 axios、 less、 VForm3
- 流程设计器<br />
1、节点设置（包括审批人、发起人、抄送人、条件设置）<br />
2、节点新增、删除、修改<br />
3、条件节点：自定义配置条件<br />
4、审批人节点：人员、角色、部门选择<br />
5、错误校验<br />
6、流程保存<br />
7、流程发布 

## 演示图 
<table>
    <tr>
        <td style="width:500px"><img src="https://gitee.com/ldhnet/AntFlow-Vue3/raw/master/public/images/4.png"/></td>  
    </tr>  
    <tr> 
        <td style="width:500px"><img src="https://gitee.com/ldhnet/AntFlow-Vue3/raw/master/public/images/2.png"/></td>
    </tr> 
    <tr>
        <td style="width:500px"><img src="https://gitee.com/ldhnet/AntFlow-Vue3/raw/master/public/images/1-4.png"/></td> 
    </tr> 
</table>
  
#### 项目安装

#### 项目运行 node14.20.1 以上版本
> 1.环境依赖 `npm  install  --registry=https://registry.npmmirror.com`

> 2.本地运行 `npm run dev` 

> 3.打包运行 `npm run build` 

##  捐赠支持
😀 你可以请作者喝杯咖啡表示鼓励
<table>
    <tr>
        <td><img src="https://gitee.com/ldhnet/AntFlow-Vue3/raw/master/public/images/wxpay.jpg"/></td>
        <td><img src="https://gitee.com/ldhnet/AntFlow-Vue3/raw/master/public/images/alipay.jpg"/></td>
    </tr>  
</table>